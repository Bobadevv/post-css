var gulp = require('gulp'),
    watch = require('gulp-watch'),
    postcss    = require('gulp-postcss'),
    precss = require('precss'),
    autoprefixer = require('autoprefixer'),
    lost = require('lost'),
    fonts = require('postcss-font-magician')({hosted: 'src/fonts'}),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    map = require('map-stream'),
    notify = require("gulp-notify"),
    scss = require('postcss-scss');

gulp.task('compileCss', function () {
        var processors = [
        precss,
        autoprefixer,
        lost,
        fonts
    ];
    return gulp.src('src/css/**/main.css')
        .pipe( sourcemaps.init() )
        .pipe(postcss(processors, {syntax: scss}))
        .pipe( sourcemaps.write('.') )
        .pipe( gulp.dest('dist/css/') )
        .pipe(notify({message: 'CSS compilation complete', onLast: true}));
});

gulp.task('compileJs', function (done) {
    return gulp.src('src/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'))
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'))
        .pipe(notify("JS compilation complete"), done);
});

gulp.task('compileVendorJs', function () {
    return gulp.src(['src/vendor/js/lib/*.js', 'src/vendor/js/plugins/*.js'])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'))
        .pipe(notify({message: 'JS compilation complete', onLast: true}));
});

gulp.task('compileVendorCss', function () {
    return gulp.src('src/vendor/css/**/*.css')
        .pipe(concatCss("/vendor.css"))
        .pipe(gulp.dest('dist/css'))
        .pipe(notify({message: 'Vendor CSS compilation complete', onLast: true}));
});

gulp.task('watch', function () {
    gulp.watch('src/sass/**/*.scss', ['compileCss']);
    gulp.watch('src/js/**/*.js', ['compileJs'])
    gulp.watch('src/vendor/js/*.js', ['compileVendorJs'])
    gulp.watch('src/vendor/css/**/*.css', ['compileVendorCss'])
});